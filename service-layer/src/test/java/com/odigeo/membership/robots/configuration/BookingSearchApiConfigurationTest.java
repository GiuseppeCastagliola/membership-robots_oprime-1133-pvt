package com.odigeo.membership.robots.configuration;

import bean.test.BeanTest;

public class BookingSearchApiConfigurationTest extends BeanTest<BookingSearchApiConfiguration> {

    private static final String PWD = "password";
    private static final String USER = "user";

    @Override
    protected BookingSearchApiConfiguration getBean() {
        BookingSearchApiConfiguration bookingApiConfiguration = new BookingSearchApiConfiguration();
        bookingApiConfiguration.setPassword(PWD);
        bookingApiConfiguration.setUser(USER);
        return bookingApiConfiguration;
    }
}
