package com.odigeo.membership.robots.expiration;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.report.Report;
import com.odigeo.membership.robots.report.Reporter;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.verification.VerificationMode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipExpirationServiceBeanTest {
    private static final VerificationMode TWO_TIMES = times(2);
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private Reporter reporter;
    private static final LocalDateTime CUTOVER = LocalDate.of(2019, Month.OCTOBER, 1).atStartOfDay();
    private MembershipExpirationServiceBean membershipExpirationServiceBean;
    private static final MembershipDTO.Builder MEMBERSHIP_DTO_BUILDER = MembershipDTO.builder();
    private static final MembershipDTO MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_BUILDER.id(1L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_BUILDER.id(2L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID3 = MEMBERSHIP_DTO_BUILDER.id(3L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID4 = MEMBERSHIP_DTO_BUILDER.id(4L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID5 = MEMBERSHIP_DTO_BUILDER.id(5L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID6 = MEMBERSHIP_DTO_BUILDER.id(6L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID7 = MEMBERSHIP_DTO_BUILDER.id(7L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID8_FAILED_EXPIRY = MEMBERSHIP_DTO_BUILDER.id(8L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID9_NULL_RESPONSE = MEMBERSHIP_DTO_BUILDER.id(9L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID1 = MEMBERSHIP_DTO_BUILDER.id(1L).balance(BigDecimal.TEN).activationDate(CUTOVER).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID2 = MEMBERSHIP_DTO_BUILDER.id(2L).balance(BigDecimal.ZERO).activationDate(CUTOVER.plusMonths(1L)).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID3 = MEMBERSHIP_DTO_BUILDER.id(3L).balance(BigDecimal.TEN).activationDate(CUTOVER.minusMonths(1L)).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID4 = MembershipDTO.builder().id(4L).activationDate(CUTOVER).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID5 = MembershipDTO.builder().id(5L).balance(BigDecimal.TEN).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID6 = MembershipDTO.builder().id(6L).balance(BigDecimal.TEN).activationDate(CUTOVER).build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID7 = MembershipDTO.builder().id(7L).balance(BigDecimal.TEN).activationDate(CUTOVER).status("ACTIVATED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID8_FAILED = MembershipDTO.builder().id(8L).balance(BigDecimal.TEN).activationDate(CUTOVER).status("EXPIRED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_EXPIRED_NULL_RESPONSE = null;
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_1 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID1).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_2 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID2).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_3 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID3).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_4 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID4).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_5 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID5).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_6 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID6).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_SUCCESS_7 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_ID7).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_FAIL = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(FAIL).response(MEMBERSHIP_DTO_EXPIRED_ID8_FAILED).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> EXPIRE_NULL_RESPONSE = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_EXPIRED_NULL_RESPONSE).build();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipExpirationServiceBean = new MembershipExpirationServiceBean(externalModuleManager, reporter);
    }


    @Test
    public void testExpireOnlyRenewableMemberships() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(false));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any(MembershipDTO.class))).will(answerSuccessAndFail(true));
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(true));
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID1))).willReturn(EXPIRE_SUCCESS_1);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID2))).willReturn(EXPIRE_SUCCESS_2);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID4))).willReturn(EXPIRE_SUCCESS_4);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID5))).willReturn(EXPIRE_SUCCESS_5);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID6))).willReturn(EXPIRE_SUCCESS_6);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID7))).willReturn(EXPIRE_SUCCESS_7);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY))).willReturn(EXPIRE_FAIL);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID9_NULL_RESPONSE))).willReturn(EXPIRE_NULL_RESPONSE);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        thenShouldTryToCreatePendingToCollect();
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should().processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID3);
        then(reporter).should().generateReportLog(any(Report.class));
    }

    private void thenShouldTryToCreatePendingToCollect() {
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID3);
    }

    @Test
    public void testExpireOnlyNotRenewableMemberships() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(true));
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(false));
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID1))).willReturn(EXPIRE_SUCCESS_1);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID2))).willReturn(EXPIRE_SUCCESS_2);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID3))).willReturn(EXPIRE_SUCCESS_3);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID4))).willReturn(EXPIRE_SUCCESS_4);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID5))).willReturn(EXPIRE_SUCCESS_5);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID6))).willReturn(EXPIRE_SUCCESS_6);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID7))).willReturn(EXPIRE_SUCCESS_7);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY))).willReturn(EXPIRE_FAIL);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID9_NULL_RESPONSE))).willReturn(EXPIRE_NULL_RESPONSE);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        then(externalModuleManager).should(never()).createPendingToCollectFromPreviousMembership(any(MembershipDTO.class));
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should().processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID3));
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testDoNotProcessRemnantFeeMemberships() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(true));
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(false));
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID1))).willReturn(EXPIRE_SUCCESS_1);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID2))).willReturn(EXPIRE_SUCCESS_2);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID3))).willReturn(EXPIRE_SUCCESS_3);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID4))).willReturn(EXPIRE_SUCCESS_4);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID5))).willReturn(EXPIRE_SUCCESS_5);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID6))).willReturn(EXPIRE_SUCCESS_6);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID7))).willReturn(EXPIRE_SUCCESS_7);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY))).willReturn(EXPIRE_FAIL);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID9_NULL_RESPONSE))).willReturn(EXPIRE_NULL_RESPONSE);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        then(externalModuleManager).should().processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID3));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID4));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID5));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID6));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID7));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID8_FAILED));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_NULL_RESPONSE));
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireBothRenewableOrNotMemberships() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(false));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any(MembershipDTO.class))).will(answerSuccessAndFail(true));
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(false));
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID1))).willReturn(EXPIRE_SUCCESS_1);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID2))).willReturn(EXPIRE_SUCCESS_2);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID3))).willReturn(EXPIRE_SUCCESS_3);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID4))).willReturn(EXPIRE_SUCCESS_4);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID5))).willReturn(EXPIRE_SUCCESS_5);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID6))).willReturn(EXPIRE_SUCCESS_6);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID7))).willReturn(EXPIRE_SUCCESS_7);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY))).willReturn(EXPIRE_FAIL);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID9_NULL_RESPONSE))).willReturn(EXPIRE_NULL_RESPONSE);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(TWO_TIMES).expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should(TWO_TIMES).expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(TWO_TIMES).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID3));
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireBothRenewableOrNotMembershipsAllSuccess() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(false));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any(MembershipDTO.class))).will(answerSuccessAndFail(false));
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(false));
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID1))).willReturn(EXPIRE_SUCCESS_1);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID2))).willReturn(EXPIRE_SUCCESS_2);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID3))).willReturn(EXPIRE_SUCCESS_3);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID4))).willReturn(EXPIRE_SUCCESS_4);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID5))).willReturn(EXPIRE_SUCCESS_5);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID6))).willReturn(EXPIRE_SUCCESS_6);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID7))).willReturn(EXPIRE_SUCCESS_7);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY))).willReturn(EXPIRE_FAIL);
        given(externalModuleManager.expireMembership(eq(MEMBERSHIP_DTO_ID9_NULL_RESPONSE))).willReturn(EXPIRE_NULL_RESPONSE);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(TWO_TIMES).expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should(TWO_TIMES).expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should(TWO_TIMES).expireMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(TWO_TIMES).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID3));
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireOnlyRenewableFailsMemberships() {
        given(externalModuleManager.retrieveRenewableMemberships()).willReturn(buildRenewableList(false));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any(MembershipDTO.class))).will(answerOnlyFail());
        given(externalModuleManager.retrieveNotRenewableMemberships()).willReturn(buildNotRenewableList(true));
        //WHEN
        membershipExpirationServiceBean.expireMemberships();
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID3);
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID1));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID2));
        then(externalModuleManager).should(never()).processRemnantFee(eq(MEMBERSHIP_DTO_EXPIRED_ID3));
        then(reporter).should().generateReportLog(any(Report.class));
    }

    private Answer<ApiCallWrapper<MembershipDTO, MembershipDTO>> answerSuccessAndFail(boolean withFailed) {
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> dtoApiCallWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.CREATE_PENDING_TO_COLLECT);
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_1 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID1).request(MEMBERSHIP_DTO_ID1).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_2 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID2).request(MEMBERSHIP_DTO_ID2).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_4 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID4).request(MEMBERSHIP_DTO_ID4).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_5 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID5).request(MEMBERSHIP_DTO_ID5).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_6 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID6).request(MEMBERSHIP_DTO_ID6).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreate = dtoApiCallWrapperBuilder.result(withFailed ? FAIL : SUCCESS).response(MEMBERSHIP_DTO_ID3).request(MEMBERSHIP_DTO_ID3).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_7 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID7).request(MEMBERSHIP_DTO_ID7).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_8 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY).request(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> successCreate_9 = dtoApiCallWrapperBuilder.result(SUCCESS).response(MEMBERSHIP_DTO_ID9_NULL_RESPONSE).request(MEMBERSHIP_DTO_ID9_NULL_RESPONSE).build();

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            if (MEMBERSHIP_DTO_ID3.equals(requestDto)) {
                return failedCreate;
            } else if (MEMBERSHIP_DTO_ID1.equals(requestDto)) {
                return successCreate_1;
            } else if (MEMBERSHIP_DTO_ID2.equals(requestDto)) {
                return successCreate_2;
            } else if (MEMBERSHIP_DTO_ID4.equals(requestDto)) {
                return successCreate_4;
            } else if (MEMBERSHIP_DTO_ID5.equals(requestDto)) {
                return successCreate_5;
            } else if (MEMBERSHIP_DTO_ID6.equals(requestDto)) {
                return successCreate_6;
            } else if (MEMBERSHIP_DTO_ID7.equals(requestDto)) {
                return successCreate_7;
            } else if (MEMBERSHIP_DTO_ID8_FAILED_EXPIRY.equals(requestDto)) {
                return successCreate_8;
            } else if (MEMBERSHIP_DTO_ID9_NULL_RESPONSE.equals(requestDto)) {
                return successCreate_9;
            }
            return null;
        };
    }

    private Answer<ApiCallWrapper<MembershipDTO, MembershipDTO>> answerOnlyFail() {
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> dtoApiCallWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.CREATE_PENDING_TO_COLLECT);
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateOne = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID1).request(MEMBERSHIP_DTO_ID1).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateTwo = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID2).request(MEMBERSHIP_DTO_ID2).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateThree = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID3).request(MEMBERSHIP_DTO_ID3).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateFour = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID4).request(MEMBERSHIP_DTO_ID4).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateFive = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID5).request(MEMBERSHIP_DTO_ID5).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateSix = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID6).request(MEMBERSHIP_DTO_ID6).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateSeven = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID7).request(MEMBERSHIP_DTO_ID7).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateEight = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY).request(MEMBERSHIP_DTO_ID8_FAILED_EXPIRY).build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> failedCreateNine = dtoApiCallWrapperBuilder.result(FAIL).response(MEMBERSHIP_DTO_ID9_NULL_RESPONSE).request(MEMBERSHIP_DTO_ID9_NULL_RESPONSE).build();

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            if (MEMBERSHIP_DTO_ID1.equals(requestDto)) {
                return failedCreateOne;
            } else if (MEMBERSHIP_DTO_ID2.equals(requestDto)) {
                return failedCreateTwo;
            } else if (MEMBERSHIP_DTO_ID3.equals(requestDto)) {
                return failedCreateThree;
            } else if (MEMBERSHIP_DTO_ID4.equals(requestDto)) {
                return failedCreateFour;
            } else if (MEMBERSHIP_DTO_ID5.equals(requestDto)) {
                return failedCreateFive;
            } else if (MEMBERSHIP_DTO_ID6.equals(requestDto)) {
                return failedCreateSix;
            } else if (MEMBERSHIP_DTO_ID7.equals(requestDto)) {
                return failedCreateSeven;
            } else if (MEMBERSHIP_DTO_ID8_FAILED_EXPIRY.equals(requestDto)) {
                return failedCreateEight;
            } else if (MEMBERSHIP_DTO_ID9_NULL_RESPONSE.equals(requestDto)) {
                return failedCreateNine;
            }
            return null;
        };
    }

    private ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>> buildNotRenewableList(boolean isEmpty) {
        return (ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>>) booleanApiCallWrapperFunction.apply(isEmpty);
    }

    private ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> buildRenewableList(boolean isEmpty) {
        return (ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO>) booleanApiCallWrapperFunction.apply(isEmpty);
    }

    private final Function<Boolean, ApiCallWrapper<List<MembershipDTO>, ?>> booleanApiCallWrapperFunction = isEmptyList -> {
        final ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO> callWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS);
        if (!isEmptyList) {
            return callWrapperBuilder.response(Arrays.asList(
                    MEMBERSHIP_DTO_ID1,
                    MEMBERSHIP_DTO_ID2,
                    MEMBERSHIP_DTO_ID3,
                    MEMBERSHIP_DTO_ID4,
                    MEMBERSHIP_DTO_ID5,
                    MEMBERSHIP_DTO_ID6,
                    MEMBERSHIP_DTO_ID7,
                    MEMBERSHIP_DTO_ID8_FAILED_EXPIRY,
                    MEMBERSHIP_DTO_ID9_NULL_RESPONSE
            )).build();
        }
        return callWrapperBuilder.build();
    };
}
