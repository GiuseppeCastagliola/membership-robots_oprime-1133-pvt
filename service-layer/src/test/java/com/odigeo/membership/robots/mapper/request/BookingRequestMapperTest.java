package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.manager.bookingapi.SearchBookingsRequestBuilder;
import org.apache.commons.lang3.BooleanUtils;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class BookingRequestMapperTest {
    private static final BookingRequestMapper BOOKING_REQUEST_MAPPER = Mappers.getMapper(BookingRequestMapper.class);

    @Test
    public void testNullMapper() {
        assertNull(BOOKING_REQUEST_MAPPER.dtoToPrimeSubscriptionBookingSummaryRequest(null));
    }

    @Test
    public void testPrimeSubscriptionBookingSummaryMapper() {
        SearchBookingsRequestBuilder searchBookingsRequestBuilder = BOOKING_REQUEST_MAPPER.dtoToPrimeSubscriptionBookingSummaryRequest(MembershipDTO.builder().id(1L).build());
        assertEquals(searchBookingsRequestBuilder.build().getMembershipId(), Long.valueOf(1L));
        assertTrue(BooleanUtils.toBoolean(searchBookingsRequestBuilder.build().getIsBookingSubscriptionPrime()));
    }
}