package com.odigeo.membership.robots.exceptions.bookingapi;

import bean.test.BeanTest;
import org.apache.commons.lang3.StringUtils;

public class BookingApiOnlyMessageExceptionTest extends BeanTest<BookingApiException> {

    @Override
    protected BookingApiException getBean() {
        return new BookingApiException(StringUtils.SPACE);
    }
}