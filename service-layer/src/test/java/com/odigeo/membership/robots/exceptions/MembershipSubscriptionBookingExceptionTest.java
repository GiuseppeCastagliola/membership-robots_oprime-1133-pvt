package com.odigeo.membership.robots.exceptions;

import bean.test.BeanTest;

public class MembershipSubscriptionBookingExceptionTest extends BeanTest<MembershipSubscriptionBookingException> {

    @Override
    protected MembershipSubscriptionBookingException getBean() {
        return new MembershipSubscriptionBookingException(new Exception());
    }
}