package com.odigeo.membership.robots.expiration;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.report.Report;
import com.odigeo.membership.robots.report.Reporter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Singleton
public class MembershipExpirationServiceBean implements MembershipExpirationService {

    private static final String EXPIRED = "EXPIRED";
    private static final LocalDateTime CUTOVER_DATE = LocalDate.of(2019, Month.SEPTEMBER, 30).atTime(LocalTime.MAX);
    private final ExternalModuleManager externalModuleManager;
    private final Reporter reporter;
    private final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> processRemnantFeeCheck = call -> call.isSuccessful()
            && isNotNullAndExpired(call.getResponse())
            && activatedAfterCutover(call.getResponse())
            && hasPositiveBalance(call.getResponse());

    @Inject
    public MembershipExpirationServiceBean(ExternalModuleManager externalModuleManager, Reporter reporter) {
        this.externalModuleManager = externalModuleManager;
        this.reporter = reporter;
    }

    @Override
    public void expireMemberships() {
        Report report = new Report();
        Stream.concat(
                getMembershipsToExpireAfterPendingToCollectCreation(searchMemberships(externalModuleManager::retrieveRenewableMemberships), report),
                searchMemberships(externalModuleManager::retrieveNotRenewableMemberships))
                .map(externalModuleManager::expireMembership)
                .peek(wrapper -> reporter.feedReport(wrapper, report))
                .filter(processRemnantFeeCheck)
                .map(callWrapper -> externalModuleManager.processRemnantFee(callWrapper.getResponse()))
                .forEach(wrapper -> reporter.feedReport(wrapper, report));
        reporter.generateReportLog(report);
    }

    private Stream<MembershipDTO> getMembershipsToExpireAfterPendingToCollectCreation(Stream<MembershipDTO> renewableMemberships, Report report) {
        return renewableMemberships.map(externalModuleManager::createPendingToCollectFromPreviousMembership)
                .peek(apiCall -> reporter.feedReport(apiCall, report))
                .filter(ApiCall::isSuccessful)
                .map(ApiCallWrapper::getRequest);
    }

    private Stream<MembershipDTO> searchMemberships(Supplier<ApiCallWrapper<List<MembershipDTO>, ?>> supplyMemberships) {
        return Optional.ofNullable(supplyMemberships.get().getResponse()).orElseGet(Collections::emptyList).stream();
    }

    private static boolean isNotNullAndExpired(MembershipDTO membership) {
        return Optional.ofNullable(membership).map(MembershipDTO::getStatus).map(EXPIRED::equals).orElse(Boolean.FALSE);
    }

    private static boolean activatedAfterCutover(MembershipDTO membership) {
        return Optional.ofNullable(membership.getActivationDate()).map(CUTOVER_DATE::isBefore).orElse(Boolean.FALSE);
    }

    private static boolean hasPositiveBalance(MembershipDTO membership) {
        return Optional.ofNullable(membership.getBalance()).map(balance -> balance.compareTo(BigDecimal.ZERO) > 0).orElse(Boolean.FALSE);
    }
}
