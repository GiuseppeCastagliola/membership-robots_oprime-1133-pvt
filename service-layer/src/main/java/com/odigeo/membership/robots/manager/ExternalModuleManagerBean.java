package com.odigeo.membership.robots.manager;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v9.requests.FeeRequest;
import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationPeriod;
import com.odigeo.membership.robots.dto.BookingDTO;
import com.odigeo.membership.robots.dto.FeeContainerDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.exceptions.MembershipSubscriptionBookingException;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.bookingapi.FeeRequestBuilder;
import com.odigeo.membership.robots.manager.bookingapi.UpdateBookingRequestBuilder;
import com.odigeo.membership.robots.manager.membership.MembershipFeeContainerTypes;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManager;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Singleton
public class ExternalModuleManagerBean implements ExternalModuleManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalModuleManagerBean.class);
    private static final String COLON = " : ";
    private static final String INVALID_PRIME_BOOKINGS_QUANTITY_EXCEPTION = "Invalid number of prime bookings (%s) found for membershipId: %s";
    private static final String NO_SUBSCRIPTION_BOOKING_DETAIL_EXCEPTION = "No subscription booking detail found for membershipId: %s";
    private static final String MULTIPLE_MEMBERSHIP_PAYMENTS_EXCEPTION = "Multiple membership payments found for membershipId: %s, bookingId: %s";
    private static final String NO_MEMBERSHIP_PAYMENTS_EXCEPTION = "No membership payments found for membershipId: %s, bookingId %s";
    private static final String MULTIPLE_FEES_EXCEPTION = "Multiple fees found for membershipId: %s, bookingId: %s, feeContainerId: %s";
    private static final String NO_FEES_EXCEPTION = "No fees found for membershipId: %s, bookingId: %s, feeContainerId: %s";
    private static final List<String> MEMBERSHIP_FEE_TYPES = Arrays.stream(MembershipFeeContainerTypes.values()).map(Enum::name).collect(Collectors.toList());
    private static final BigDecimal ONE_CENT = new BigDecimal("0.01");
    private final ExpirationPeriod expirationPeriod;
    private final MembershipOfferModuleManager membershipOfferModuleManager;
    private final MembershipModuleManager membershipModuleManager;
    private final MembershipSearchModuleManager membershipSearchModuleManager;
    private final Map<String, ApiCallWrapper<MembershipOfferDTO, MembershipDTO>> websitesOffers;
    private final BookingRequestMapper bookingRequestMapper;
    private final BookingResponseMapper bookingResponseMapper;
    private final BookingApiManager bookingApiManager;
    private final Predicate<FeeContainerDTO> hasMembershipFeeContainerType = feeContainer -> MEMBERSHIP_FEE_TYPES.contains(feeContainer.getFeeContainerType());
    private final Predicate<FeeContainerDTO> hasFeeWithSubscriptionOrRenewalSubCode = feeContainer -> feeContainer.getFeeDTOs().stream()
            .anyMatch(fee -> MembershipFeeContainerTypes.getAllSubCodes().contains(fee.getSubCode()));
    private final Predicate<FeeContainerDTO> isMembershipFeeContainer = hasMembershipFeeContainerType.or(hasFeeWithSubscriptionOrRenewalSubCode);

    @Override
    public ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> retrieveRenewableMemberships() {
        return membershipSearchModuleManager.searchMemberships(searchBuilderByPeriod().status(MembershipStatus.ACTIVATED.name()).autoRenewal("ENABLED").build());
    }

    @Inject
    public ExternalModuleManagerBean(ExpirationPeriod expirationPeriod, MembershipOfferModuleManager membershipOfferModuleManager, MembershipModuleManager membershipModuleManager, MembershipSearchModuleManager membershipSearchModuleManager,
                                     BookingRequestMapper bookingRequestMapper, BookingResponseMapper bookingResponseMapper, BookingApiManager bookingApiManager) {
        this.expirationPeriod = expirationPeriod;
        this.membershipOfferModuleManager = membershipOfferModuleManager;
        this.membershipModuleManager = membershipModuleManager;
        this.membershipSearchModuleManager = membershipSearchModuleManager;
        this.bookingRequestMapper = bookingRequestMapper;
        this.bookingResponseMapper = bookingResponseMapper;
        this.bookingApiManager = bookingApiManager;
        websitesOffers = new HashMap<>();
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollectFromPreviousMembership(MembershipDTO previousMembership) {
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> offerWrapper = getOffer(previousMembership);
        return offerWrapper.isSuccessful() ? membershipModuleManager.createPendingToCollect(previousMembership, offerWrapper.getResponse())
                : new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER).result(ApiCall.Result.FAIL)
                .response(previousMembership).exception(offerWrapper.getException()).message(offerWrapper.getMessage()).build();
    }

    private enum MembershipStatus {
        PENDING_TO_ACTIVATE, PENDING_TO_COLLECT, ACTIVATED, DEACTIVATED, EXPIRED
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO) {
        return membershipModuleManager.expireMembership(membershipDTO);
    }

    @Override
    public ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>> retrieveNotRenewableMemberships() {
        final List<SearchMembershipsDTO> searchMembershipsDTOs = Arrays.asList(
                searchDtoDisabledRenewal(),
                searchDtoDeactivated(),
                searchDtoPendingToCollect(),
                searchDtoPendingToActivate(),
                searchDtoExpiredPositiveBalance());
        final List<MembershipDTO> totalResult = searchMembershipsDTOs.stream()
                .map(membershipSearchModuleManager::searchMemberships)
                .map(ApiCallWrapper::getResponse)
                .flatMap(List::stream)
                .collect(Collectors.toList());
        return new ApiCallWrapperBuilder<List<MembershipDTO>, List<SearchMembershipsDTO>>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS)
                .request(searchMembershipsDTOs).response(totalResult).build();
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFee(MembershipDTO expiredMembership) {
        ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> processRemnantFeeWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.PROCESS_REMNANT_FEE)
                .request(expiredMembership);
        final ApiCallWrapper<BookingDTO, MembershipDTO> bookingProcessFeeWrapper = retrieveBookingAndProcessFees(expiredMembership);
        bookingProcessFeeWrapper.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, bookingProcessFeeWrapper.getEndpoint().name(), String.valueOf(expiredMembership.getId())));
        if (bookingProcessFeeWrapper.isSuccessful()) {
            consumeMembershipRemnantBalance(expiredMembership, processRemnantFeeWrapperBuilder);
        } else {
            processRemnantFeeWrapperBuilder.result(bookingProcessFeeWrapper.getResult()).response(expiredMembership)
                    .exception(bookingProcessFeeWrapper.getException()).message(bookingProcessFeeWrapper.getMessage());
        }
        ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFeeCallWrapper = processRemnantFeeWrapperBuilder.build();
        processRemnantFeeCallWrapper.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, bookingProcessFeeWrapper.getEndpoint().name(), String.valueOf(expiredMembership.getId())));
        return processRemnantFeeCallWrapper;
    }

    private ApiCallWrapper<BookingDTO, MembershipDTO> retrieveBookingAndProcessFees(MembershipDTO expiredMembership) {
        final ApiCallWrapperBuilder<BookingDTO, MembershipDTO> bookingProcessFeeWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.BOOKING_PROCESS_REMNANT_FEE)
                .request(expiredMembership);
        try {
            BookingDTO membershipSubscriptionBookingDetail = getSubscriptionBookingDetailForMembership(expiredMembership);
            MembershipFeeSubCodes membershipFeeSubCode = getMembershipFeeSubCode(membershipSubscriptionBookingDetail, expiredMembership.getId());
            UpdateBookingRequest updateBookingRequest = getUpdateBookingRequest(expiredMembership.getBalance(), membershipSubscriptionBookingDetail.getCurrencyCode(),
                    membershipFeeSubCode);
            ApiCallWrapper<BookingDTO, UpdateBookingRequest> updatedBooking = updateBooking(membershipSubscriptionBookingDetail.getId(), updateBookingRequest);
            bookingProcessFeeWrapperBuilder.result(updatedBooking.getResult()).message(updatedBooking.getMessage());
            if (Objects.nonNull(updatedBooking.getResponse())) {
                bookingProcessFeeWrapperBuilder.response(updatedBooking.getResponse());
            }
            if (Objects.nonNull(updatedBooking.getException())) {
                bookingProcessFeeWrapperBuilder.exception(updatedBooking.getException());
            }
        } catch (MembershipSubscriptionBookingException e) {
            bookingProcessFeeWrapperBuilder.result(ApiCall.Result.FAIL)
                    .response(BookingDTO.builder().membershipId(expiredMembership.getId()).build())
                    .exception(e).message(e.getMessage());
        }
        return bookingProcessFeeWrapperBuilder.build();
    }

    private void consumeMembershipRemnantBalance(MembershipDTO expiredMembership, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> processRemnantFeeWrapperBuilder) {
        ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantBalanceWrapper = membershipModuleManager.consumeRemnantMembershipBalance(expiredMembership);
        consumeRemnantBalanceWrapper.logResultAndMessageWithPrefix(LOGGER, String.join(COLON, consumeRemnantBalanceWrapper.getEndpoint().name(), String.valueOf(expiredMembership.getId())));
        processRemnantFeeWrapperBuilder.result(consumeRemnantBalanceWrapper.getResult())
                .response(consumeRemnantBalanceWrapper.getResponse())
                .exception(consumeRemnantBalanceWrapper.getException())
                .message(consumeRemnantBalanceWrapper.getMessage());
    }

    private BookingDTO getSubscriptionBookingDetailForMembership(MembershipDTO membership) throws MembershipSubscriptionBookingException {
        BookingDTO primeSubscriptionBookingSummary = getPrimeSubscriptionBookingSummaryForMembership(membership);
        BookingDTO primeSubscriptionBookingDetail = getBookingDetailFromBookingSummary(primeSubscriptionBookingSummary);
        return bookingResponseMapper.bookingResponsesToDto(primeSubscriptionBookingSummary, primeSubscriptionBookingDetail);
    }

    private BookingDTO getPrimeSubscriptionBookingSummaryForMembership(MembershipDTO membership) throws MembershipSubscriptionBookingException {
        final SearchBookingsRequest primeSubscriptionBookingSummaryRequest = bookingRequestMapper.dtoToPrimeSubscriptionBookingSummaryRequest(membership).build();
        final ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(
                ApiCall.Endpoint.SEARCH_BOOKINGS).request(primeSubscriptionBookingSummaryRequest);

        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchPrimeBookingSummaries = bookingApiManager.searchBookings(bookingSummariesWrapperBuilder);

        handleFailedCallExceptions(searchPrimeBookingSummaries);
        handleInvalidPrimeSubscriptionBookingsQuantity(searchPrimeBookingSummaries, membership.getId());
        return searchPrimeBookingSummaries.getResponse().get(0);
    }

    private BookingDTO getBookingDetailFromBookingSummary(BookingDTO primeSubscriptionBookingSummary) throws MembershipSubscriptionBookingException {
        final ApiCallWrapperBuilder<BookingDTO, BookingDTO> primeSubscriptionBookingDetailWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
                .request(primeSubscriptionBookingSummary);
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDetailCallWrapper = bookingApiManager.getBooking(primeSubscriptionBookingDetailWrapperBuilder);
        handleFailedCallExceptions(bookingDetailCallWrapper);
        if (Objects.isNull(bookingDetailCallWrapper.getResponse())) {
            throw new MembershipSubscriptionBookingException(String.format(NO_SUBSCRIPTION_BOOKING_DETAIL_EXCEPTION, primeSubscriptionBookingSummary.getMembershipId()));
        }
        return bookingDetailCallWrapper.getResponse();
    }

    private <U, T> void handleFailedCallExceptions(ApiCallWrapper<U, T> callWrapper) throws MembershipSubscriptionBookingException {
        if (callWrapper.getResult().equals(ApiCall.Result.FAIL)) {
            if (Objects.nonNull(callWrapper.getException())) {
                throw new MembershipSubscriptionBookingException(callWrapper.getMessage(), callWrapper.getException());
            }
            throw new MembershipSubscriptionBookingException(callWrapper.getMessage());
        }
    }

    private void handleInvalidPrimeSubscriptionBookingsQuantity(ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchPrimeBookingSummaries, long membershipId)
            throws MembershipSubscriptionBookingException {
        if (searchPrimeBookingSummaries.isSuccessful() && searchPrimeBookingSummaries.getResponse().size() != 1) {
            String errorMessage = String.format(INVALID_PRIME_BOOKINGS_QUANTITY_EXCEPTION, searchPrimeBookingSummaries.getResponse().size(), membershipId);
            throw new MembershipSubscriptionBookingException(errorMessage);
        }
    }

    private ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long id, UpdateBookingRequest updateBookingRequest) {
        final ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(
                ApiCall.Endpoint.UPDATE_BOOKING).request(updateBookingRequest);
        return bookingApiManager.updateBooking(id, updateBookingWrapperBuilder);
    }

    private UpdateBookingRequest getUpdateBookingRequest(BigDecimal balance, String currencyCode, MembershipFeeSubCodes membershipFeeSubCode) {
        FeeRequest negatedFee = new FeeRequestBuilder().withAmount(balance.negate()).withCurrency(currencyCode).withSubCode(membershipFeeSubCode).build();
        FeeRequest remnantFee = new FeeRequestBuilder().withAmount(balance).withCurrency(currencyCode).withSubCode(MembershipFeeSubCodes.AC12).build();
        return new UpdateBookingRequestBuilder().withFeeRequests(Arrays.asList(negatedFee, remnantFee)).build();
    }

    private MembershipFeeSubCodes getMembershipFeeSubCode(BookingDTO membershipSubscriptionBookingDetail, long expiredMembershipId) throws MembershipSubscriptionBookingException {
        long subscriptionBookingId = membershipSubscriptionBookingDetail.getId();
        List<FeeContainerDTO> membershipFeeContainer = membershipSubscriptionBookingDetail.getFeeContainers().stream().filter(isMembershipFeeContainer)
                .collect(Collectors.toList());
        if (membershipFeeContainer.size() != 1 || membershipFeeContainer.get(0).getFeeDTOs().size() != 1) {
            handleFeeSubCodeException(expiredMembershipId, subscriptionBookingId, membershipFeeContainer);
        }
        FeeContainerDTO feeContainer = membershipFeeContainer.get(0);
        return feeContainer.getFeeContainerType() == null ? MembershipFeeSubCodes.valueOf(feeContainer.getFeeDTOs().get(0).getSubCode())
                : MembershipFeeContainerTypes.valueOf(feeContainer.getFeeContainerType()).getSubCode();
    }

    private void handleFeeSubCodeException(long expiredMembershipId, long subscriptionBookingId, List<FeeContainerDTO> membershipFeeContainer)
            throws MembershipSubscriptionBookingException {
        String errorMessage;
        if (membershipFeeContainer.size() != 1) {
            errorMessage = membershipFeeContainer.isEmpty() ? String.format(NO_MEMBERSHIP_PAYMENTS_EXCEPTION, expiredMembershipId, subscriptionBookingId)
                    : String.format(MULTIPLE_MEMBERSHIP_PAYMENTS_EXCEPTION, expiredMembershipId, subscriptionBookingId);
        } else {
            FeeContainerDTO feeContainer = membershipFeeContainer.get(0);
            errorMessage = feeContainer.getFeeDTOs().size() <= 1 ? String.format(NO_FEES_EXCEPTION, expiredMembershipId, subscriptionBookingId, feeContainer.getId())
                    : String.format(MULTIPLE_FEES_EXCEPTION, expiredMembershipId, subscriptionBookingId, feeContainer.getId());
        }
        throw new MembershipSubscriptionBookingException(errorMessage);
    }

    private SearchMembershipsDTO searchDtoPendingToCollect() {
        return searchBuilderByPeriod().status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
    }


    private ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getOffer(MembershipDTO previousMembership) {
        return websitesOffers.computeIfAbsent(previousMembership.getWebsite(), wrap -> membershipOfferModuleManager.getOffer(previousMembership));
    }

    private SearchMembershipsDTO.SearchMembershipsDTOBuilder searchBuilderByPeriod() {
        LocalDate startingFrom = StringUtils.isEmpty(expirationPeriod.getStartingFrom()) ? LocalDate.now() : LocalDate.parse(expirationPeriod.getStartingFrom());
        return SearchMembershipsDTO.builder()
                .withMemberAccount(false).toExpirationDate(startingFrom)
                .fromExpirationDate(startingFrom.minusDays(expirationPeriod.getDaysInThePast()));
    }

    private SearchMembershipsDTO searchDtoDeactivated() {
        return searchBuilderByPeriod().status(MembershipStatus.DEACTIVATED.name()).build();
    }

    private SearchMembershipsDTO searchDtoDisabledRenewal() {
        return searchBuilderByPeriod().status(MembershipStatus.ACTIVATED.name()).autoRenewal("DISABLED").build();
    }

    private SearchMembershipsDTO searchDtoPendingToActivate() {
        return searchBuilderByPeriod().status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();
    }

    private SearchMembershipsDTO searchDtoExpiredPositiveBalance() {
        return searchBuilderByPeriod().minBalance(ONE_CENT)
                .fromActivationDate(LocalDate.of(2019, Month.OCTOBER, 1))
                .status(MembershipStatus.EXPIRED.name()).build();
    }
}
