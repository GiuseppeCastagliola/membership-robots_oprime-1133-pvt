package com.odigeo.membership.robots.manager;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;

import java.util.List;

public interface ExternalModuleManager {
    ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollectFromPreviousMembership(MembershipDTO previousMembership);

    ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> retrieveRenewableMemberships();

    ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>> retrieveNotRenewableMemberships();

    ApiCallWrapper<MembershipDTO, MembershipDTO> processRemnantFee(MembershipDTO expiredMembership);
}
