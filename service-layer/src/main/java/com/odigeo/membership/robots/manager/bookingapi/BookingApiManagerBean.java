package com.odigeo.membership.robots.manager.bookingapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v9.BadCredentialsException;
import com.odigeo.bookingapi.v9.BookingApiService;
import com.odigeo.bookingapi.v9.InvalidParametersException;
import com.odigeo.bookingapi.v9.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v9.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.BookingApiConfiguration;
import com.odigeo.membership.robots.configuration.BookingSearchApiConfiguration;
import com.odigeo.membership.robots.dto.BookingDTO;
import com.odigeo.membership.robots.exceptions.bookingapi.BookingApiException;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Collections.emptyList;

@Singleton
public class BookingApiManagerBean implements BookingApiManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingApiManager.class);
    private static final String BOOKING_API_ERROR_MSG = "Error in BookingApi module when executing: %s for bookingId: %s";
    private static final String SEARCH_BOOKINGS_ERROR_MESSAGE = "Error in BookingSearchApi module when executing searchBookings()";
    private static final String GET_BOOKING_SUCCESS_MESSAGE = "%s booking found for userId %s";
    private static final String SEARCH_BOOKINGS_SUCCESS_MESSAGE = "%s %s bookingSummaries found for membershipId %s";
    private static final String UPDATE_BOOKING_SUCCESS_MESSAGE = "updateBooking successfully completed for bookingId: %s";
    private static final String PRIME = "Prime";
    private static final String NON_PRIME = "non-Prime";
    private static final String COLON_DELIMITER = " : ";

    private final BookingApiService bookingApiService;
    private final BookingSearchApiService bookingSearchApiService;
    private final BookingApiConfiguration bookingApiConfiguration;
    private final BookingSearchApiConfiguration bookingSearchApiConfiguration;
    private final BookingResponseMapper bookingResponseMapper;

    @Inject
    public BookingApiManagerBean(BookingApiService bookingApiService, BookingSearchApiService bookingSearchApiService, BookingApiConfiguration bookingApiConfiguration,
                                 BookingSearchApiConfiguration bookingSearchApiConfiguration, BookingResponseMapper bookingResponseMapper) {
        this.bookingApiService = bookingApiService;
        this.bookingSearchApiService = bookingSearchApiService;
        this.bookingApiConfiguration = bookingApiConfiguration;
        this.bookingSearchApiConfiguration = bookingSearchApiConfiguration;
        this.bookingResponseMapper = bookingResponseMapper;
    }

    @Override
    public ApiCallWrapper<BookingDTO, BookingDTO> getBooking(ApiCallWrapperBuilder<BookingDTO, BookingDTO> bookingDetailWrapperBuilder) {
        long bookingId = bookingDetailWrapperBuilder.getRequest().getId();
        LOGGER.info("BookingApiManager::getBooking with bookingId {}", bookingId);

        try {
            BookingDetail bookingDetail = bookingApiService.getBooking(bookingApiConfiguration.getUser(), bookingApiConfiguration.getPassword(), Locale.getDefault(), bookingId);
            BookingDTO booking = bookingResponseMapper.bookingDetailResponseToDto(bookingDetail);

            if (successfulCall(bookingDetail)) {
                String successMessage = format(GET_BOOKING_SUCCESS_MESSAGE, booking.getId(), booking.getUserId());
                bookingDetailWrapperBuilder.result(ApiCall.Result.SUCCESS).response(booking).message(successMessage);
            } else {
                bookingDetailWrapperBuilder.result(ApiCall.Result.FAIL).response(booking);
                Optional.ofNullable(bookingDetail).ifPresent(bookingDetailResponse -> {
                    Optional.ofNullable(bookingDetailResponse.getMessage()).ifPresent(bookingDetailWrapperBuilder::message);
                    Optional.ofNullable(bookingDetailResponse.getErrorMessage()).ifPresent(bookingDetailWrapperBuilder::message);
                });
            }

        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            String errorMessage = format(BOOKING_API_ERROR_MSG, "getBooking()", bookingId);
            LOGGER.error(errorMessage, e);
            bookingDetailWrapperBuilder.result(ApiCall.Result.FAIL).exception(new BookingApiException(errorMessage, e)).message(exceptionMessage(errorMessage, e));
        }

        final ApiCallWrapper<BookingDTO, BookingDTO> result = bookingDetailWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), String.valueOf(bookingId)));
        return result;
    }

    @Override
    public ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings(ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder) {
        SearchBookingsRequest searchBookingsRequest = bookingSummariesWrapperBuilder.getRequest();
        LOGGER.info("BookingApiManager::searchBookings with SearchBookingsRequest {}", searchBookingsRequest);
        String bookingType = Boolean.parseBoolean(searchBookingsRequest.getIsBookingSubscriptionPrime()) ? PRIME : NON_PRIME;
        try {
            SearchBookingsPageResponse<List<BookingSummary>> searchBookingsResponse = bookingSearchApiService
                    .searchBookings(bookingSearchApiConfiguration.getUser(), bookingSearchApiConfiguration.getPassword(), Locale.getDefault(), searchBookingsRequest);

            //null response object should pass null to the DTO mapper, but a null getBookings() result can be an empty list.
            List<BookingDTO> bookingSummaries = Objects.isNull(searchBookingsResponse) ? bookingResponseMapper.bookingSummariesResponseToDto(null)
                    : bookingResponseMapper.bookingSummariesResponseToDto(Optional.ofNullable(searchBookingsResponse.getBookings()).orElse(emptyList()));

            if (successfulCall(searchBookingsResponse)) {
                String successMessage = format(SEARCH_BOOKINGS_SUCCESS_MESSAGE, bookingSummaries.size(), bookingType, searchBookingsRequest.getMembershipId());
                bookingSummariesWrapperBuilder.result(ApiCall.Result.SUCCESS).response(bookingSummaries).message(successMessage);
            } else {
                bookingSummariesWrapperBuilder.result(ApiCall.Result.FAIL).response(bookingSummaries);
                Optional.ofNullable(searchBookingsResponse)
                        .flatMap(response -> Optional.ofNullable(response.getErrorMessage()))
                        .ifPresent(bookingSummariesWrapperBuilder::message);
            }
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            LOGGER.error(SEARCH_BOOKINGS_ERROR_MESSAGE, e);
            bookingSummariesWrapperBuilder.result(ApiCall.Result.FAIL).exception(new BookingApiException(SEARCH_BOOKINGS_ERROR_MESSAGE, e))
                    .message(exceptionMessage(SEARCH_BOOKINGS_ERROR_MESSAGE, e));
        }

        final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> result = bookingSummariesWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), bookingType, String.valueOf(result.getRequest().getMembershipId())));
        return result;
    }

    @Override
    public ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long bookingId, ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> apiCallWrapperBuilder) {
        UpdateBookingRequest updateBookingRequest = apiCallWrapperBuilder.getRequest();
        LOGGER.info("BookingApiManager::updateBooking with bookingId {}, updateBookingRequest {}", bookingId, updateBookingRequest);
        try {
            BookingDetail bookingDetail = bookingApiService
                    .updateBooking(bookingApiConfiguration.getUser(), bookingApiConfiguration.getPassword(), Locale.getDefault(), bookingId, updateBookingRequest);
            BookingDTO booking = bookingResponseMapper.bookingDetailResponseToDto(bookingDetail);

            if (successfulCall(bookingDetail)) {
                apiCallWrapperBuilder.result(ApiCall.Result.SUCCESS).response(booking).message(format(UPDATE_BOOKING_SUCCESS_MESSAGE, bookingId));
            } else {
                apiCallWrapperBuilder.result(ApiCall.Result.FAIL).response(booking);
                Optional.ofNullable(bookingDetail).ifPresent(bookingDetailResponse -> {
                    Optional.ofNullable(bookingDetail.getMessage()).ifPresent(apiCallWrapperBuilder::message);
                    Optional.ofNullable(bookingDetail.getErrorMessage()).ifPresent(apiCallWrapperBuilder::message);
                });
            }
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            String errorMessage = format(BOOKING_API_ERROR_MSG, "updateBooking()", bookingId);
            LOGGER.error(errorMessage, e);
            apiCallWrapperBuilder.result(ApiCall.Result.FAIL).exception(new BookingApiException(errorMessage, e)).message(exceptionMessage(errorMessage, e));
        }

        final ApiCallWrapper<BookingDTO, UpdateBookingRequest> result = apiCallWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), String.valueOf(bookingId)));
        return result;
    }

    private boolean successfulCall(com.odigeo.bookingsearchapi.v1.responses.BaseResponse response) {
        return Objects.nonNull(response) && Objects.isNull(response.getErrorMessage());
    }

    private boolean successfulCall(com.odigeo.bookingapi.v9.responses.BaseResponse response) {
        return Objects.nonNull(response) && Objects.isNull(response.getErrorMessage());
    }

    private String exceptionMessage(String customErrorMessage, Exception e) {
        return String.join(System.lineSeparator(), customErrorMessage, e.getMessage());
    }
}
