package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.Collections;

public class MemberAccountDTOTest extends BeanTest<MemberAccountDTO> {

    public static final long TEST_ID = 1L;
    public static final String LAST_NAME = "Perez";
    public static final String NAME = "Sancho";

    @Override
    protected MemberAccountDTO getBean() {
        return MemberAccountDTO.builder()
                .id(TEST_ID)
                .lastNames(LAST_NAME)
                .memberships(Collections.singletonList(MembershipDTO.builder().build()))
                .userId(TEST_ID)
                .name(NAME)
                .build();
    }

    @Test
    public void memberAccountDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(MemberAccountDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }


}