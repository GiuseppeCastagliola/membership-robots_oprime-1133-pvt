package com.odigeo.membership.robots.dto;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.unmodifiableList;

public class BookingDTO {
    private final long id;
    private final long userId;
    private final long membershipId;
    private final List<FeeContainerDTO> feeContainers;
    private final String currencyCode;

    private BookingDTO(Builder builder) {
        id = builder.id;
        userId = builder.userId;
        membershipId = builder.membershipId;
        feeContainers = builder.feeContainers;
        currencyCode = builder.currencyCode;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public long getMembershipId() {
        return membershipId;
    }

    public List<FeeContainerDTO> getFeeContainers() {
        return unmodifiableList(feeContainers);
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long id;
        private long userId;
        private long membershipId;
        private List<FeeContainerDTO> feeContainers;
        private String currencyCode;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder userId(long userId) {
            this.userId = userId;
            return this;
        }

        public Builder membershipId(long membershipId) {
            this.membershipId = membershipId;
            return this;
        }

        public Builder feeContainers(List<FeeContainerDTO> feeContainerDTOs) {
            this.feeContainers = feeContainerDTOs;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public BookingDTO build() {
            return new BookingDTO(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingDTO that = (BookingDTO) o;
        return id == that.id && userId == that.userId && membershipId == that.membershipId && Objects.equals(feeContainers, that.feeContainers)
                && Objects.equals(currencyCode, that.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, membershipId, feeContainers, currencyCode);
    }
}
